<?php

class Finances
{
	//Constructeur
	function __construct ($capitalisation, $valeurEntreprise, $per, $rendement, $capiCa, $VeCa, 
		$VeEbitda, $courSurActif, $ca, $ebitda, $margeExploit, $margeNette, $dividendeAction, $levier ,
		 $tresorerie , $roe , $capexCa )
	{
		$this->capitalisation = $capitalisation; 
		$this->valeurEntreprise = $valeurEntreprise; 
		$this->per = $per; 
		$this->rendement = $rendement; 
		$this->capiCa = $capiCa; 
		$this->VeCa = $VeCa; 
		$this->VeEbitda = $VeEbitda; 
		$this->courSurActif = $courSurActif; 
		$this->ca = $ca;
		$this->ebitda = $ebitda;
		$this->margeExploit = $margeExploit;
		$this->margeNette = $margeNette;
		$this->dividendeAction = $dividendeAction;
		$this->levier = $levier;
		$this->tresorerie = $tresorerie;
		$this->roe = $roe;
		$this->capexCa = $capexCa;
	}

	//attributs #lafusée
	public $capitalisation;
	public $valeurEntreprise;
	public $per;
	public $rendement;
	public $capiCa;
	public $VeCa;
	public $VeEbitda;
	public $courSurActif;

	public $ca;
	public $ebitda;
	public $margeExploit;
	public $margeNette;
	public $dividendeAction;

	public $levier;
	public $tresorerie;
	public $roe;
	public $capexCa;

} 
